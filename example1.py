# Momentan doar vom incerca un exemplu pentru acest libsvm

import numpy as np
import matplotlib.pyplot as plt

from ... import libsvm.svmutil.*


def circle(radius, sigma=0, num_points=50):
    t = np.linspace(0, 2*np.pi, num_points)
    d = np.zeros((num_points,2), dtype=np.float)
    d[:,0] = radius*np.cos(t) + np.random.randn(t.size)*sigma
    d[:,1] = radius*np.sin(t) + np.random.randn(t.size)*sigma
    return d

num_train = 100
num_test = 30
sigma = 0.2

d1 = circle(3, sigma, num_train)
d2 = circle(5, sigma, num_train)

plt.figure()
plt.plot(d1[:,0],d1[:,1],'ro')
plt.plot(d2[:,0],d2[:,1],'bo')
plt.show()

# training data
c_train = []
c_train.extend([0]*num_train)
c_train.extend([1]*num_train)
d_train = np.vstack((circle(3,sigma,num_train), circle(5,sigma,num_train))).tolist()

# test data
c_test = []
c_test.extend([0]*num_test)
c_test.extend([1]*num_test)
d_test = np.vstack((circle(3,sigma,num_test), circle(5,sigma,num_test))).tolist()

problem = svm_problem(c_train,d_train)

param = svm_parameter("-q") # quiet!
param.kernel_type=RBF

m = svm_train(problem, param)

pred_lbl, pred_acc, pred_val = svm_predict(c_test,d_test,m)
